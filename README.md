This project loads the control function from https://arxiv.org/abs/2009.13438 and a control I produced myself. The system (based on the article) is then initialized and the initial wavefunction is propagated using the two different controls. Finally, information such as the fidelities, the potentials and the wavefunctions as a function of time is saved to .mat files.

Suggestion to run the project:
Change the settings.txt file appropriately. 
When this is done, run the following commands while in the main folder:

mkdir build;
cd build;
cmake ..;
make;

The project should now be build. The .cpp file is located in "programs/seed_evolution". This is also the location of the executable, "run". 

The main folder has an input folder, which includes .mat files to load the states and the seeds. When the executable has been run, the output is saved in the output folder (also in the main folder).

Note: I made this project using a template I made, so some folder/files in the main folder are currently superfluous (the test and src folders for instance).
