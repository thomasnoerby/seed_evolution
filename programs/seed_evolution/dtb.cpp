#include "qengine/qengine.h"
#include <iostream>

using namespace qengine;

int main()
{
	// creating system
	const auto xMin = -2;
	const auto xMax = 2;
	const auto x0 = 0.0; // only used to initialize V;
	const auto nx = 256;
	const auto dt = 0.002;
	const auto kinFactor = 0.5;
	const auto g = 1.6198; // corresponds to N = 700 atoms

	const auto duration_thomas = 1.0525; // simulation time
	const auto duration_article = 1.0525;

	const auto space = gpe::makeHilbertSpace(xMin,xMax,nx,kinFactor);
	const auto x = space.x();

	const auto V_func = [&x](const real u)
	{
		const auto x2 = x * x; // from article
		const auto x4 = x2 * x2;
		const auto x6 = x4 * x2;
		const auto w_0 = 0.7307;
		const auto div_factor = w_0/(2*PI);

		real Rf = u; // the control is called Rf in the article
		real Rf2 = Rf * Rf;
		real Rf3 = Rf2 * Rf;
		real Rf4 = Rf3 * Rf;
		real Rf5 = Rf4 * Rf;
		real Rf6 = Rf5 * Rf;

		// real a0 = 2 * PI * (54.451 - 8.6264*Rf + 3570.3*Rf2 - 12650*Rf3 + 25646*Rf4 - 27546*Rf5 + 12106*Rf6); // not included, as it is constant over all x
		real a2 = (74.025 - 19.429*Rf - 3309.1*Rf2 + 18497.0*Rf3 - 46369.0*Rf4 + 56311.0*Rf5 - 26894.0*Rf6);
		real a4 = (-3.4221 + 24.648*Rf + 1231.6*Rf2 - 8450.8*Rf3 + 23425.0*Rf4 - 30416.0*Rf5 + 15268.0*Rf6);
		real a6 = (0.2406 - 6.0581*Rf - 153.85*Rf2 + 1221.2*Rf3 - 3661.4*Rf4 + 5049.5*Rf5 - 2663.3*Rf6);

		auto V = (a2 * x2 + a4 * x4 + a6 * x6)/div_factor;
		return V;
	};

	const auto K = space.T(); // kinetic term
	const auto V = makePotentialFunction(V_func,x0); // potential
	const auto meanfield = makeGpeTerm(g);
	const auto H = K+V+meanfield; // Hamiltonian


	// Loading
	std::string input_folder = "../../input/";

	// loading Thomas' control seed
	std::string seed_str_thomas = std::string(input_folder + "thomas_u_opt_F=0.99999_tampered.json");
	DataContainer seed_container_thomas;
	seed_container_thomas.load(seed_str_thomas);
	const auto seed_vec_thomas = seed_container_thomas.getRVec("u_opt");
	const Control u_thomas = makeControl({seed_vec_thomas},dt);

	// loading article control seed
	std::string seed_str_article = std::string(input_folder + "article_seed_dt=0.002_T=1.0525.json");
	DataContainer seed_container_article;
	seed_container_article.load(seed_str_article);
	const auto seed_vec_article = seed_container_article.getRVec("u_opt");
	const Control u_article = makeControl({seed_vec_article},dt);

	// loading states
	// xMin = -2, xMax = 2, nx = 256, N = 700 (nr. of atoms), G = 2.5041
	std::string state_0 = std::string(input_folder + "psi0.json");
	DataContainer state_0_container;
	state_0_container.load(state_0);
	const auto psi0_vec = state_0_container.getRVec("psi0");

	std::string state_2 = std::string(input_folder + "psi2.json");
	DataContainer state_2_container;
	state_2_container.load(state_2);
	const auto psi2_vec = state_2_container.getRVec("psi2");


	const auto psi0 = makeFunctionOfX(space._serializedHilbertSpace(), CVec{ psi0_vec, 0.0 * psi0_vec}); // converting to QEngine states
	const auto psi2 = makeFunctionOfX(space._serializedHilbertSpace(), CVec{ psi2_vec, 0.0 * psi2_vec});

	// propagation
	auto stepper = makeFixedTimeStepper(H, psi0, dt);

	const auto propagateOverControl = [&](auto& u, std::string fieldname, const auto duration)
	{
		const auto nt = floor(duration/dt)+1;
		DataContainer dc;

		stepper.reset(psi0,u.getFront());
		dc["u_" + fieldname].append(u.mat());
		dc["psi0"].append(psi0.vec());
		dc["psi2"].append(psi2.vec());
		dc["nt_" + fieldname] = nt;
		dc["duration_" + fieldname] = duration;

		for(auto i = 0; i < nt; i++)
		{
			const auto& psi = normalize(stepper.state());
			dc["V_" + fieldname].append(V(u.get(i)).vec());
			dc["psi_" + fieldname].append(psi.vec());
			dc["overlap_" + fieldname].append(overlap(psi,psi2));
			dc["fidelity_" + fieldname].append(fidelity(psi,psi2));
			if(i < nt-1) stepper.step(u.get(i+1));
		}

		// variances
		dc["psi0_variance"] = variance(H(0.3),psi0);
		dc["psi2_variance"] = variance(H(0.51),psi2);

		std::string saveStr = std::string("../../output/" + fieldname + ".json");
		dc.save(saveStr);
	};

	propagateOverControl(u_thomas,"thomas_control",duration_thomas);
	propagateOverControl(u_article,"article_control",duration_article);

	return 0;
}
